<?php

namespace Moodle\MoodleMagento\Controller\Adminhtml;

use Magento\Backend\App\Action;
use Moodle\MoodleMagento\Model\ConfigurationRepository;
use Magento\Framework\Registry;
/**
 * Admin Front abstract controller
 */
abstract class AbstractController extends Action
{

    /**
     *
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    protected $configurationRepository;
    /**
     * @var Registry
     */
    protected $coreRegistry;
    /**
     * @param Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param configurationRepository $frontRepository
     * @param Registry $registry
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        ConfigurationRepository $configurationRepository,
        Registry $registry
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->configurationRepository = $configurationRepository;
        $this->coreRegistry = $registry;
        parent::__construct($context);
    }

}