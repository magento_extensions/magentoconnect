<?php

namespace Moodle\MoodleMagento\Controller\Adminhtml\Index;


use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;

    /*public function __construct(Context $context,PageFactory $resultPageFactory)
    {
        parent::__construct($context);
            $this->resultPageFactory = $resultPageFactory;
    }*/

    public function execute()
    {
       /* $resultPage = $this->resultFactory->create(ResultFactory::TYPE_RAW);
        $resultPage->setContents('Hello Admins');*/
        //$resultPage->getConfig()->getTitle()->prepend((__('Moodle Categories Mapping')));

        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        return $resultPage;
    }


}