<?php

namespace Moodle\MoodleMagento\Controller\Adminhtml\ManageMoodleCategories;


use Magento\Framework\Controller\ResultFactory;

class Index extends \Magento\Backend\App\Action
{
    protected $resultPageFactory = false;

    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory)
    {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }
    public function execute()
    {
        $resultPage = $this->resultFactory->create(ResultFactory::TYPE_PAGE);
        //$resultPage->setContents('Hello Admins');
        $resultPage->getConfig()->getTitle()->prepend((__('Sync Moodle Categories')));

        return $resultPage;
    }


}