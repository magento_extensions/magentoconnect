<?php

namespace Moodle\MoodleMagento\Controller\Adminhtml\Configuration;

use Moodle\MoodleMagento\Controller\Adminhtml\AbstractController;

use Magento\Backend\App\Action;
use Magento\Framework\Api\FilterBuilderFactory;
use Magento\Framework\Api\Search\FilterGroupBuilderFactory;
use Magento\Framework\Api\SearchCriteriaBuilderFactory;
use Magento\Framework\App\ResponseInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;
use Moodle\MoodleMagento\Model\Client as MoodleClient;
use Moodle\MoodleMagento\Api\ConfigurationRepositoryInterface;
use Moodle\MoodleMagento\Model\ConfigurationFactory;
use Moodle\MoodleMagento\Api\ProductsRepositoryInterface;
use Moodle\MoodleMagento\Model\ProductsFactory;


class Save extends Action
{



    protected $collectionFactory;
    protected $resultPageFactory;
    protected $registry;
    private $filterBuilderFactory;
    private $filterGroupBuilderFactory;
    private $searchCriteriaBuilderFactory;
    private $configurationRepository;
    private $configurationFactory;
    private $apiData;
    private $productsFactory;
    private $productsRepository;


    public function __construct(
        Action\Context $context,
        ConfigurationRepositoryInterface $configurationRepository,
        FilterBuilderFactory $filterBuilderFactory,
        FilterGroupBuilderFactory $filterGroupBuilderFactory,
        SearchCriteriaBuilderFactory $searchCriteriaBuilderFactory,
        ConfigurationFactory $configurationFactory,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Registry $registry,
        \Moodle\MoodleMagento\Model\ResourceModel\Configuration\CollectionFactory $collectionFactory,
        MoodleClient $apiData,
        ProductsRepositoryInterface $productsRepository,
        ProductsFactory $productsFactory
    ) {
        parent::__construct($context);
        $this->resultPageFactory=$resultPageFactory;
        $this->configurationRepository = $configurationRepository;
        $this->registry=$registry;
        $this->collectionFactory=$collectionFactory;
        $this->filterBuilderFactory = $filterBuilderFactory;
        $this->filterGroupBuilderFactory = $filterGroupBuilderFactory;
        $this->searchCriteriaBuilderFactory = $searchCriteriaBuilderFactory;
        $this->configurationFactory = $configurationFactory;
        $this->apiData=$apiData;
        $this->productsRepository=$productsRepository;
        $this->productsFactory=$productsFactory;



        //$this->dataObjectHelper=$dataObjectHelper;

    }

    public function execute()
    {

        try {
           /* $id=1;
            if ($id) {
                $config = $this->configurationRepository->getById($id);
            } else {
                $config = $this->configurationFactory->create();
            }
            $config->setMoodleUrl($this->getRequest()->getPostValue()['general']['moodle_url']);
            $config->setMoodleWebservice($this->getRequest()->getPostValue()['general']['moodle_webservice']);
            $config->setMoodleToken($this->getRequest()->getPostValue()['general']['moodle_token']);*/

            $config=$this->getRequest()->getPostValue()['general'];
            $this->configurationFactory->create()->updateMoodleInstance($config);

            $getCoursesData=$this->configurationFactory->create()->storeDataInProductTable();
            $moodleUrl=$getCoursesData['moodle_url'];
            $moodleToken=$getCoursesData['moodle_token'];
            $moodleresponse=$this->apiData->MoodleData($moodleUrl,$moodleToken);
            //var_dump($moodleresponse);
            /*
             * It works properly
             * $saveCourses=$this->productsFactory->create()->getResource()->getConnection()->select()
             *
             */
            foreach($moodleresponse as $singlecourseDetails) {
                $this->productsFactory->create()->setData($singlecourseDetails)->save();
            }
            /**
             * @return array Products in Products from moodle_magento_products
             */
            //$saveCourses=$this->productsRepository->getList();
            // configurationRepository not redirecting
            //$moodleresponse=$this->configurationRepository->apiCaller();

            //$config->storeDataInProductTable();
            $this->messageManager->addSuccess(__('Configuration successfully added'));


        } catch (\Exception $e) {
            $this->messageManager->addError(__($e->getMessage()));
        }




        return $this->resultRedirectFactory->create()->setPath('moodlemagento/managemoodleconfiguration/index');

    }
}
