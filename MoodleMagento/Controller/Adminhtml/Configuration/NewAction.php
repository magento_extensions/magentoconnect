<?php

namespace Moodle\MoodleMagento\Controller\Adminhtml\Configuration;

use Magento\Framework\Controller\ResultFactory;

class NewAction extends \Magento\Backend\App\Action
{
    public function execute()
    {
        $resultPage=$this->resultFactory->create(ResultFactory::TYPE_PAGE);
        $resultPage->getConfig()->getTitle()->set(__('Adding Moodle Configuration'));
        return $resultPage;

    }
}
