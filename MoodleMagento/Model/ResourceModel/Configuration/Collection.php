<?php

namespace Moodle\MoodleMagento\Model\ResourceModel\Configuration;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;
use Moodle\MoodleMagento\Model\Configuration;
use Moodle\MoodleMagento\Model\ResourceModel\Configuration as ResourceConfiguration;

class Collection extends AbstractCollection
{
    protected $_idFieldName='id';

    protected function _construct()
    {
        //   parent::_construct(); // TODO: Change the autogenerated stub
        $this->_init(
            Configuration::class,
            ResourceConfiguration::class

        );
    }
}