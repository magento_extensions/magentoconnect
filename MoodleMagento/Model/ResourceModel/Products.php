<?php

namespace Moodle\MoodleMagento\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Products extends AbstractDb
{
    protected function _construct()
    {
        // TODO: Implement _construct() method.
        $this->_init('moodle_magento_products','id');

    }
}