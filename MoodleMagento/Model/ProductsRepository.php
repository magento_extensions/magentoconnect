<?php

namespace Moodle\MoodleMagento\Model;

use Moodle\MoodleMagento\Api\ProductsRepositoryInterface;
use Moodle\MoodleMagento\Model\ResourceModel\Products\CollectionFactory;

class ProductsRepository implements ProductsRepositoryInterface
{
    private $collectionFactory;
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory=$collectionFactory;
    }

    public function getList()
    {
        // TODO: Implement getList() method.
        /**
         * getItems() list of object with data other function
         * getData() gives data which is present in Database
         */
        //$this->collectionFactory->create()->getItems();
        return $this->collectionFactory->create()->getItems();
    }
    public function save(ProductsRepositoryInterface $object){
        try {
            $object->save();
        } catch (Exception $e) {
            throw new CouldNotSaveException($e->getMessage());
        }
        return $object;
    }
}
