<?php

namespace Moodle\MoodleMagento\Model;

use Moodle\MoodleMagento\Api\Data\ConfigurationInterface;
use Moodle\MoodleMagento\Api\ConfigurationRepositoryInterface;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Moodle\MoodleMagento\Model\Client as MoodleClient;

use Moodle\MoodleMagento\Model\ResourceModel\Configuration\CollectionFactory;


class ConfigurationRepository implements ConfigurationRepositoryInterface
{
    private $collectionFactory;
    protected $configurationFactory;
    protected $objectFactory;
    protected $apiData;
    public function __construct(ConfigurationFactory $objectFactory,
                                CollectionFactory $collectionFactory,
                                MoodleClient $apiData



)
    {
        $this->objectFactory=$objectFactory;
        $this->collectionFactory=$collectionFactory;
        $this->apiData=$apiData;

    }

    public function getList(SearchCriteriaInterface $criteria)
    {
        // TODO: Implement getList() method.
        return $this->collectionFactory->create()->getItems();
    }
    public function save(ConfigurationInterface $object)
    {
        try {
            $object->save();
        } catch (Exception $e) {
            throw new CouldNotSaveException($e->getMessage());
        }
        return $object;
    }
    public function getById($id)
    {
        $object = $this->objectFactory->create();
        $object->load($id);
        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Object with id "%1" does not exist.', $id));
        }
        return $object;
    }

    public function getDataStored(ConfigurationInterface $object){

        try {
            $object->getStoredData();
        } catch (Exception $e) {
            throw new CouldNotSaveException($e->getMessage());
        }
        return $object;
    }

    public function update(ConfigurationInterface $object,ConfigurationInterface $data)
    {

        try {
           $object->delete();
        } catch (Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }
    /*public function updateInstance(){
        $data=$this->collectionFactory->create()
            ->addFieldToFilter('id',10);
        var_dump($data);
        die();

    }*/
    /*public function setMoodleCourses(){
        var_dump();
        die();
        $courses=$this->apiData->getCourses();
    }*/
    public function updateById($id,$data)
    {

        return $this->update($this->getById($id),$data);
    }
    public function apiCaller(){
        var_dump("I am in API client calling function");
        $Api=$this->apiData->MoodleData('http://devmd31.3esofttech.com','2e76815be74faf3eb5e1bb1b5f2c0390');
        var_dump($Api);
        die();
        return $Api;
    }



}
