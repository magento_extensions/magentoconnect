<?php

namespace Moodle\MoodleMagento\Model;

use Moodle\MoodleMagento\Api\CategoriesRepositoryInterface;
use Moodle\MoodleMagento\Model\ResourceModel\Categories\CollectionFactory;

class CategoriesRepository implements CategoriesRepositoryInterface
{
    private $collectionFactory;
    public function __construct(CollectionFactory $collectionFactory)
    {
        $this->collectionFactory=$collectionFactory;
    }

    public function getList()
    {
        // TODO: Implement getList() method.
        return $this->collectionFactory->create()->getItems();
    }
}
