<?php
/**
 * Taxjar_SalesTax
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 *
 * @category   Taxjar
 * @package    Taxjar_SalesTax
 * @copyright  Copyright (c) 2017 TaxJar. TaxJar is a trademark of TPS Unlimited, Inc. (http://www.taxjar.com)
 * @license    http://opensource.org/licenses/osl-3.0.php Open Software License (OSL 3.0)
 */

namespace Moodle\MoodleMagento\Model;

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Exception\LocalizedException;
//use Taxjar\SalesTax\Model\Configuration as TaxjarConfig;

class Client
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var string
     */
    protected $storeZip;

    /**
     * @var string
     */
    protected $storeRegionCode;

    /**
     * @var \Magento\Directory\Model\CountryFactory
     */
    protected $regionFactory;

    /**
     * @var bool
     */
    protected $showResponseErrors;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param RegionFactory $regionFactory
     */
    public function __construct(

    ) {

    }

    /**
     * Perform a GET request
     *
     * @param string $resource
     * @param array $errors
     * @return array
     */
    public function getResource($resource, $errors = [])
    {
        $client = $this->getClient($this->_getApiUrl($resource));
        return $this->_getRequest($client, $errors);
    }

    /**
     * Perform a POST request
     *
     * @param string $resource
     * @param array $data
     * @param array $errors
     * @return array
     */
    public function MoodleData($resource, $token,$data=null, $errors = [])
    {
        var_dump($resource);
        $url = 'http://devmd31.3esofttech.com/webservice/rest/server.php?wstoken=2e76815be74faf3eb5e1bb1b5f2c0390&wsfunction=core_course_get_courses&moodlewsrestformat=json';
        $data = array();

        $options = array('http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultCoursesObject=json_decode($result);
        $data=array();
        foreach ($resultCoursesObject as $key=>$resultSingleCourseObject) {
            $data[$key]['course_id']=$resultSingleCourseObject->id;
            $data[$key]['product_id']=$resultSingleCourseObject->id;
            $data[$key]['course_name']=$resultSingleCourseObject->fullname;
            /*$data{$key}->course_id=$resultSingleCourseObject->id;
            $data{$key}->product_id=$resultSingleCourseObject->id;
            $data{$key}->course_name=$resultSingleCourseObject->fullname;*/
        }

            /*if($course->id!=1){

                $course = $DB->get_record('course', array('id'=> $course->id));
                if(!empty($course->enablecompletion)){
                    $courseData[] = array('id'=>$course->id,'name'=>$course->fullname);
                }

            }*/
        //}
        //return $courseData;
        /*var_dump($resultParse);
        die();*/

       /* $client = $this->getClient($this->_getApiUrl($resource,$token), \Zend_Http_Client::POST);
        $client->setRawData(json_encode($data), 'application/json');
        var_dump($client);
        die();*/

        return $data;

    }
    /**
     * Get SmartCalcs API URL
     *
     * @param string $resource
     * @return string
     */
    private function _getApiUrl($resource,$token)
    {
        // $apiUrl = TaxjarConfig::TAXJAR_API_URL;

        $apiUrl = $resource;
        $apiUrl.='/webservice/rest/server.php?wstoken=';
        $apiUrl.=$token;
        $apiUrl.='wsfunction=core_course_get_courses';
        $apiUrl='moodlewsrestformat=json';

        return $apiUrl;
    }
    /**
     * Get HTTP request
     *
     * @param \Zend_Http_Client $client
     * @param array $errors
     * @return array
     * @throws LocalizedException
     */
    private function _getRequest($client, $errors = [])
    {
        try {
            $response = $client->request();

            if ($response->isSuccessful()) {
                $json = $response->getBody();
                return json_decode($json, true);
            } else {
                $this->_handleError($errors, $response);
            }
        } catch (\Zend_Http_Client_Exception $e) {
            throw new LocalizedException(__('Could not connect to Moodle.'));
        }
    }

    /**
     * Get HTTP Client
     *
     * @param string $url
     * @param string $method
     * @return \Zend_Http_Client $client
     */
    private function getClient($url, $method = \Zend_Http_Client::GET)
    {
        // @codingStandardsIgnoreStart
        $client = new \Zend_Http_Client($url, ['timeout' => 30]);
        // @codingStandardsIgnoreEnd
        $client->setUri($url);
        $client->setMethod($method);
        $client->setHeaders('Content-Type', 'application/x-www-form-urlencoded');

        return $client;
    }






    /**
     * Get HTTP Client
     *
     * @param string $url
     * @param string $method
     * @return \Zend_Http_Client $client
     */
    private function getMoodleResponse($url,$token, $method = \Zend_Http_Client::POST)
    {
        $url = $url;
        $webservice='/webservice/rest/server.php?wstoken=';
        $get_token=$token;
        $get_Courses_api='wsfunction=core_course_get_courses';
        $moodleRest='moodlewsrestformat=json';


        $options = array('http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
        );
        $context  = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultParse=json_decode($result);

        foreach($resultParse as $userDetails){
            $userEmails[]=$userDetails->email;
        }

        // @codingStandardsIgnoreStart
        $client = new \Zend_Http_Client($url, ['timeout' => 30]);
        // @codingStandardsIgnoreEnd
        $client->setUri($url);
        $client->setMethod($method);
        //$client->setHeaders('Authorization', 'Bearer ' . $this->apiKey);
        $client->setHeaders('Content-Type', 'application/x-www-form-urlencoded');
        return $client;
    }







}
