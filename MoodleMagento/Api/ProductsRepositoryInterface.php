<?php

namespace Moodle\MoodleMagento\Api;

interface ProductsRepositoryInterface
{
    /**
     * @return \Moodle\MoodleMagento\Api\Data\ProductsInterfce[]
    */
    public function getList();
}