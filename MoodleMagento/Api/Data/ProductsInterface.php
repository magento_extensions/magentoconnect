<?php

namespace Moodle\MoodleMagento\Api\Data;

interface ProductsInterface
{
    public function getProduct_id();
    public function getCourse_name();
    public function getStatus();
}