<?php

namespace Moodle\MoodleMagento\Api\Data;

interface ConfigurationInterface
{

    const MOODLE_URL='moodle_url';
    const MOODLE_WEBSERVICE='moodle_webservice';
    const MOODLE_TOKEN='moodle_token';

    public function getMoodleUrl();
    public function getMoodleWebservice();
    public function getMoodleToken();

    public function setMoodleUrl($moodleUrl);
    public function setMoodleWebservice($moodleWebservice);
    public function setMoodleToken($moodleToken);

}