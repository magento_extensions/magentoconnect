<?php

namespace Moodle\MoodleMagento\Api\Data;

interface CategoriesInterface
{
    public function getMoodleCategoryName();
    public function getMagentoCategoryName();
    public function getStatus();
}