<?php

namespace Moodle\MoodleMagento\Api;

interface CategoriesRepositoryInterface
{
    /**
     * @return \Moodle\MoodleMagento\Api\Data\CategoriesInterfce[]
     */
    public function getList();
}