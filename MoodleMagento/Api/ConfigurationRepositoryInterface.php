<?php

namespace Moodle\MoodleMagento\Api;

use Moodle\MoodleMagento\Api\Data\ConfigurationInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

interface ConfigurationRepositoryInterface
{
    /**
     * Returns menus list
     *
     * @api
     * @param \Magento\Framework\Api\SearchCriteriaInterface $criteria
     * @return \Snowdog\Menu\Api\Data\MenuSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $criteria);

    public function save(ConfigurationInterface $data);

    public function getById($id);


    public function update(ConfigurationInterface $object,ConfigurationInterface $data);

    public function updateById($id,$data);
    public function apiCaller();




}