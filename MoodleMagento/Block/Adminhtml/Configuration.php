<?php

namespace Moodle\MoodleMagento\Block\Adminhtml;

use Magento\Framework\View\Element\Template;
use Moodle\MoodleMagento\Model\ResourceModel\Configuration\Collection;
use Moodle\MoodleMagento\Model\ResourceModel\Configuration\CollectionFactory;

class Configuration extends Template
{
    public function __construct(Template\Context $context,
                                CollectionFactory $collectionFactory,
                                array $data=[])
    {
        $this->collectionFactory=$collectionFactory;
        parent::__construct($context,$data); // TODO: Change the autogenerated stub
    }
    /**
    @return \Moodle\MoodleMagento\Model\Product[]
     */
    public function getConfiguration(){
        $page= $this->collectionFactory->create()->getItems();
        //var_dump($page);
        return $page;
    }
}