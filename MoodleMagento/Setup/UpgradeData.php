<?php
/**
 * Created by PhpStorm.
 * User: Arshad
 * Date: 1/23/2019
 * Time: 11:13 PM
 */

namespace Moodle\MoodleMagento\Setup;


use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\UpgradeDataInterface;

class UpgradeData implements UpgradeDataInterface
{

    /**
     * Upgrades data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        // TODO: Implement upgrade() method.
        //echo __METHOD__.PHP_EOL;
        $setup->startSetup();
        if(version_compare($context->getVersion(),'1.0.1','<')){
            $setup->getConnection()->update(
                $setup->getTable('moodle_magento_products'),
                [
                    'status'=>'synced'
                ],
                $setup->getConnection()->quoteInto('id=?',1)
            );

            $setup->getConnection()->update(
                $setup->getTable('moodle_magento_categories'),
                [
                    'status'=>'mapped'
                ],
                $setup->getConnection()->quoteInto('id=?',1)
            );

        }

        if(version_compare($context->getVersion(),'1.0.5','<')){
            $this->addConfiguration($setup);
        }
        $setup->endSetup();

    }
    private function addConfiguration(ModuleDataSetupInterface $setup){
        $setup->getConnection()->insert(
            $setup->getTable('moodle_magento_configuration'),
            [   'moodle_url'=>'http://localhost/moodle35/?rest',
                'moodle_webservice'=>'core_get_course',
                'moodle_token'=>'89wyhuihwyiurwoow'
            ]
        );
    }
}