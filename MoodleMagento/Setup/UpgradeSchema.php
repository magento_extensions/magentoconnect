<?php
/**
 * Created by PhpStorm.
 * User: Arshad
 * Date: 1/23/2019
 * Time: 11:12 PM
 */

namespace Moodle\MoodleMagento\Setup;


use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\UpgradeSchemaInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{

    /**
     * Upgrades DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // TODO: Implement upgrade() method.
        //echo __METHOD__.PHP_EOL;
        $setup->startSetup();
        if(version_compare($context->getVersion(),'1.0.1','<')){
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable('moodle_magento_products'),
                    'status',
                    [
                        'type'=> Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment'=> 'product Sync']
                );
            $setup->getConnection()
                ->addColumn(
                    $setup->getTable('moodle_magento_categories'),
                    'status',
                    [
                        'type'=> Table::TYPE_TEXT,
                        'nullable' => true,
                        'comment'=> 'Categories mapped']
                );
        }
        if(version_compare($context->getVersion(), '1.0.5', '<')) {
            $this->addNewTable($setup);
        }




        $setup->endSetup();
    }
    private function addNewTable(SchemaSetupInterface $setup)
    {
        $table=$setup->getConnection()->newTable(
            $setup->getTable('moodle_magento_configuration')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity'=>true,'nullable'=>false,'primary'=>true]
        )->addColumn(
            'moodle_url',
            Table::TYPE_TEXT,
            255,
            ['nullable'=>false],
            'Moodle URL'
        )->addColumn(
            'moodle_webservice',
            Table::TYPE_TEXT,
            255,
            ['nullable'=>false],
            'Moodle Webservice'
        )->addColumn(
            'moodle_token',
            Table::TYPE_TEXT,
            255,
            ['nullable'=>false],
            'Moodle Token'
        )->addIndex(
            $setup->getIdxName(
                'moodle_magento_configuration',
                ['moodle_url']),
            ['moodle_url']
        )->setComment('Moodle configuration');

        $setup->getConnection()->createTable($table);


        return $this;
    }
}