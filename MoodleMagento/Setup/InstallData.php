<?php
/**
 * Created by 3e.
 * User: Arshad
 * Date: 1/23/2019
 * Time: 11:03 PM
 */

namespace Moodle\MoodleMagento\Setup;


use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{

    /**
     * Installs data for a module
     *
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        // TODO: Implement install() method.
        //echo __METHOD__.PHP_EOL;
        $setup->startSetup();
        $setup->getConnection()->insert(
            $setup->getTable('moodle_magento_products'),
            [   'product_id'=>92,
                'course_id'=>34,
                'course_name'=>'moodle_LTI'
            ]
        );
        $setup->getConnection()->insert(
            $setup->getTable('moodle_magento_products'),
            [
                'product_id'=>93,
                'course_id'=>35,
                'course_name'=>'moodle_Scorm'
            ]
        );

        $setup->getConnection()->insert(
            $setup->getTable('moodle_magento_categories'),
            [   'magento_category_name'=>'category-1',
                'moodle_category_name'=>'Web Development'

            ]
        );
        $setup->getConnection()->insert(
            $setup->getTable('moodle_magento_categories'),
            [   'magento_category_name'=>'category-2',
                'moodle_category_name'=>'Web Development'
            ]
        );

        $setup->endSetup();
    }
}