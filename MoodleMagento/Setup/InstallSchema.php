<?php
/**
 * Created by PhpStorm.
 * User: Arshad
 * Date: 1/23/2019
 * Time: 11:01 PM
 */

namespace Moodle\MoodleMagento\Setup;


use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{

    /**
     * Installs DB schema for a module
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @return void
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        // TODO: Implement install() method.
        //echo __METHOD__.PHP_EOL;

        $setup->startSetup();

        $table=$setup->getConnection()->newTable(
            $setup->getTable('moodle_magento_categories')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity'=>true,'nullable'=>false,'primary'=>true]
        )->addColumn(
            'magento_category_name',
            Table::TYPE_TEXT,
            255,
            ['nullable'=>false],
            'Magento Categories'
        )->addColumn(
            'moodle_category_name',
            Table::TYPE_TEXT,
            255,
            ['nullable'=>false],
            'Moodle Categories'
        )->addIndex(
            $setup->getIdxName(
                'moodle_magento_categories',
                ['moodle_category_name']),
            ['moodle_category_name']
        )->setComment('Sample Categories');

        $setup->getConnection()->createTable($table);


        $table=$setup->getConnection()->newTable(
            $setup->getTable('moodle_magento_products')
        )->addColumn(
            'id',
            Table::TYPE_INTEGER,
            null,
            ['identity'=>true,'nullable'=>false,'primary'=>true]
        )->addColumn(
            'product_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable'=>false]
        )->addColumn(
            'course_id',
            Table::TYPE_INTEGER,
            null,
            ['nullable'=>false]
        )->addColumn(
            'course_name',
            Table::TYPE_TEXT,
            255,
            ['nullable'=>false],
            'Moodle Courses'
        )->addIndex(
            $setup->getIdxName(
                'moodle_magento_products',
                ['course_name']),
            ['course_name']
        )->setComment('Sample Products');

        $setup->getConnection()->createTable($table);

        $setup->endSetup();
    }
}